from flask import Flask, request, jsonify
import subprocess
import os.path
from settings import *
app = Flask(__name__)

@app.route('/')
def indes():
    return 'The logbook REST API is running.'

@app.route('/log', methods=['POST'])
def post_to_logbook():
    if not os.path.isfile(LOG_BIN):
        return jsonify({'Result':'Server Error, file not found'}), 500;
    
    required = ['user', 'password', 'text', 'logbook', 'system', 'author']
    #json_req = request.get_json()
    json_req = dict((k.lower(),v) for k,v in request.get_json().items())
    
    if not json_req:
        abort(400)
    for key in required:
        if not key in json_req:
            return jsonify({'Result':'Request is missing key \''+key+'\''}), 400;
    
    user = json_req.pop('user')
    password = json_req.pop('password')
    logbook = json_req.pop('logbook')
    text = json_req.pop('text')
    
    #/group/online/ecs/PVSSShortcutProjects311/GlobalProject/bin/elog -h logbook.lbdaq.cern.ch -p 8080 -l LHCb-Test -u common Common! -a System="Test" -a author="Author Name" -a Subject="Test" "This is a test message"
    command = [LOG_BIN, "-h", SERVER, "-p", PORT, "-l", logbook, "-u", user, password]
    for key in json_req:
        command.extend(['-a', key+"="+json_req[key]])
    command.extend([text])
    print command
    p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    out, err = p.communicate()

    return jsonify({'Result':out})
