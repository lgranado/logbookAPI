#!/usr/bin/python
import os
import sys

os.environ['PYTHON_EGG_CACHE'] = '/tmp'

root_path = '/home/lgranado/flask_logbook'

sys.path += [root_path]

from logbookAPI import app as application
